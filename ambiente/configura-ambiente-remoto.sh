#!/bin/sh

REPO=https://gitlab.com/ralvesnt/conchayoro.git
NODE1=ip172-18-0-42-blnelqcgl230009cdo0g@direct.labs.play-with-docker.com
NODE2=ip172-18-0-45-blnelqcgl230009cdo0g@direct.labs.play-with-docker.com
NODE3=ip172-18-0-46-blnelqcgl230009cdo0g@direct.labs.play-with-docker.com
NODE4=ip172-18-0-47-blnelqcgl230009cdo0g@direct.labs.play-with-docker.com 

echo "Configuração do ambiente remoto"
  
ssh -t $NODE1 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d jenkins"

ssh -t $NODE2 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d nexus && docker-compose exec nexus sh /deploy-config/nexus/init.sh"

ssh -t $NODE3 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d sonar" 
   
echo "Configuração do ambiente remoto concluída com sucesso!!"